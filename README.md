# Git & Fork me !

#### What you need to do?

Your task:

    - Fork this repository
    - Make update (read below what to update)
    - Create your own branch named -> yourName_yourSurname
    - Send merge request using your branch above as source branch until tomorrow 09:00

#### What you should add?

You should briefly explain why we need forking and sending merge requests.

    - Explain purpose of forking
    - Explain the purpose of merge requests
    - Explain the target and source branches in merge requests
    - Go ahead and do labs in lab.github.com regarding to merge(pull) requests

Links:

    - https://lab.github.com/githubtraining/managing-merge-conflicts
    - https://lab.github.com/githubtraining/reviewing-pull-requests

There are a lot of other labs there to strengthen your git knowledge.

***NOTE:*** No need to write essay here, just few words how you undrestood the process. That's it.

--- 

# About Fork

In general, forking copy the original repo and you can make changes and do not worry about making mistakes. Because it will not affect to original project. Especially you can use it in big projects


# About Merge requests

Merge requests uses for merging one branch to another. And helps to fix problems between teams if there is any. 
Target and source branche: Source branch we can create by ourselves, code one part of project and at the end it will be merged to target branch








